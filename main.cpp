/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Gauss-Seidel
 *    		    a simple implementation of an iterative method.
 *    		   
 *    		    Solving a simple matrix equation.
 *
 *    		    Want to proceed to solution of a simple boundary value problem by
 *    		    relaxation (based on LeVeque).
 *
 *        Version:  1.0
 *        Created:  02/07/13 11:42:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  is the war against death of the universe!
 *
 * =====================================================================================
 */

#include "main.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  carpe diem!
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    init();
    
    for(int i = 0; i < num_iterations; i++)
    	iterate();
    
    log(0);


    return EXIT_SUCCESS;
}	/* ----------  end of function main  ---------- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  init
 *  Description:  prepares the problem variables
 * =====================================================================================
 */
void init()
{
   solution = gsl_vector_alloc(num_datapoints);    // allocate the vector of solutions
   			        	           // and take an initial guess to be a 
   			        	           // nullvector.
   	      gsl_vector_set(solution, 0, 1);
   	      gsl_vector_set(solution, 1, 0);
   	      gsl_vector_set(solution, 2, 1);

   rhs = gsl_vector_alloc(num_datapoints);	   // allocate rhs
   	 gsl_vector_set(rhs, 0, 1);
   	 gsl_vector_set(rhs, 1, 28);
   	 gsl_vector_set(rhs, 2, 76);
   
   mtx = gsl_matrix_calloc(num_datapoints,num_datapoints); // allocate mtx.
  
   // This is bit of a pain. Is there a better way to do so?
   // 
   // | 12  3  -5 |
   // |  1  5   3 |
   // |  3  7   13|
   // 
   // Note: Yes, there is - use mtx->data and copy memory block of an array?
   //       TODO: try it.

   gsl_matrix_set(mtx, 0, 0, 12);
   gsl_matrix_set(mtx, 0, 1,  3);
   gsl_matrix_set(mtx, 0, 2, -5);

   gsl_matrix_set(mtx, 1, 0, 1);
   gsl_matrix_set(mtx, 1, 1, 5);
   gsl_matrix_set(mtx, 1, 2, 3);

   gsl_matrix_set(mtx, 2, 0, 3);
   gsl_matrix_set(mtx, 2, 1, 7);
   gsl_matrix_set(mtx, 2, 2, 13);
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  iterate
 *  Description:  iterates the problem, replaces solution with a new guess
 * =====================================================================================
 */
void iterate()
{    
    double* x_; 			    // local var, points to element under consideration
    double* rhs_;			    // local, points to rhs element

    for(int i = 0; i < num_datapoints; i++) // rows
    {
	  x_ = gsl_vector_ptr(solution, i);   // store the pointer to considered row element
	rhs_ = gsl_vector_ptr(rhs, 	i);   // store the ptr to rhs row element 
	
	 *x_ = *rhs_;			      // prepare _x

	 for(int j = 0; j < num_datapoints; j++)
	 {
		if(j == i)
		    continue; // skip x_

		*x_ -= gsl_matrix_get(mtx, i, j)*gsl_vector_get(solution, j);
	 }

	 *x_ /= gsl_matrix_get(mtx, i, i);  // divide by row elem
    }
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  log
 *  Description:  logs out to a file. A+ rights set. TODO use stringstream to construct
 *   		  the string with id.
 * =====================================================================================
 */
void log(int i)
{   
    FILE* f = fopen("vector.dat", "a+");
    gsl_vector_fprintf(f, solution, "%.5g");
    fclose(f);
}
