
/*-----------------------------------------------------------------------------
 *  main.h
 *-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *  INCLUDES
 *-----------------------------------------------------------------------------*/
#include	<cstdlib>
#include 	<sstream>
#include	<iostream>
#include 	<gsl/gsl_vector.h>
#include 	<gsl/gsl_linalg.h>
#include	<gsl/gsl_matrix.h>


/*-----------------------------------------------------------------------------
 *  VARIABLES
 *-----------------------------------------------------------------------------*/
int num_iterations = 10; 		      // number of gauss-seidel iterations in the problem
int num_datapoints = 3; 		      // precision of the problem
double h	   = 1/(num_datapoints + 1);  // mesh-width of the problem

gsl_vector* solution;  // k-th guess on solution to the problem 
gsl_vector* rhs;       // RHS of the problem
gsl_matrix* mtx;       // matrix of the linear system

/*-----------------------------------------------------------------------------
 *  FUNCTIONS
 *-----------------------------------------------------------------------------*/
void init(); 		// prepares the problem variables
void iterate();		// iterates the problem
void log(int i); 	// logs to a file
