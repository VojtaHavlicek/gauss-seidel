# Gauss-Seidel

PROJECT=gauss-seidel

# compiler
CC=g++

# compiler C flags
OPT=-O2 -lgsl -std=c++0x

# file list
SRC=main.cpp
OBJ=${SRC:.cpp=.o}
HEAD=${SRC:.cpp=.h}

# targets
.PHONY: build

# Make everything from sratch
build:${SRC} ${PROJECT}

# Make rule
${PROJECT}: ${OBJ}
	${CC} ${OBJ} ${OPT} -o ${PROJECT}


